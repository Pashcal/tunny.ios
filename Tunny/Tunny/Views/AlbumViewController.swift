//
//  AlbumViewController.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import UIKit
import Nuke

class AlbumViewController: UIViewController {
    
    private let lookupService = LookupService()
    
    private let album: Album!
    private let tableViewSource = AlbumTableViewSource()
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var singerLabel: UILabel!
    @IBOutlet weak private var genreLabel: UILabel!
    @IBOutlet weak private var yearLabel: UILabel!
    
    init(album: Album) {
        self.album = album
        super.init(nibName: "AlbumViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.album = nil
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Album"//album.collectionName
        
        initAlbumDetails()
        initTableView()
        
        loadSongs()
    }
    
    func initAlbumDetails() {
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 4
        
        let imageWidth = Int(imageView.bounds.width * UIScreen.main.scale)
        let imageUrl = URL(string: album.artworkUrlWith(width: imageWidth))!
        Nuke.loadImage(with: imageUrl, into: imageView)
        
        titleLabel.text = album.collectionName
        singerLabel.text = album.artistName
        genreLabel.text = album.primaryGenreName
        
        if let releaseDate = album.releaseDate {
            let year = Calendar.current.component(.year, from: releaseDate)
            yearLabel.text = String(year)
        }
        else {
            yearLabel.text = ""
        }
    }
    
    func initTableView() {
        
        tableView.register(SongTableViewCell.nib, forCellReuseIdentifier: SongTableViewCell.key)
        tableView.dataSource = tableViewSource
    }
    
    func loadSongs() {
        guard let albumId = album.collectionID else {
            return
        }
        
        lookupService.getSongs(forAlbum: albumId) { songs in
            self.tableViewSource.items = songs
            self.tableView.reloadData()
            
            self.tableView.tableFooterView = UIView()
        }
    }
}
