//
//  ApiConfig.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation

class ApiConfig {
    static let timeout: TimeInterval = 30
    static let baseUrl = "https://itunes.apple.com/"
}
