//
//  SearchService.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation

class SearchService: BaseService {
    
    let searchEndpoint = "search"
    
    func getAlbums(by searchQuery: String, complete: @escaping ([Album]?) -> Void) {
        let encodedQuery = searchQuery
            .replacingOccurrences(of: " ", with: "+")
            .addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let queryItems = [
            URLQueryItem(name: "term", value: encodedQuery),
            URLQueryItem(name: "media", value: "music"),
            URLQueryItem(name: "entity", value: "album"),
            URLQueryItem(name: "limit", value: "42")
        ]
        self.get(searchEndpoint, complete: { (data: Response<Album>?) in
            complete(data?.results.sorted(by: { (first, second) -> Bool in
                return first.collectionName?.compare(second.collectionName ?? "") == .orderedAscending
            }))
        }, queryItems: queryItems)
    }
}
