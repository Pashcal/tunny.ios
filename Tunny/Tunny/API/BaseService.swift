//
//  BaseService.swift
//  Tunny
//
//  Created by Pavel Chupryna on 11/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation

class BaseService {
    
    let urlSessionConfig: URLSessionConfiguration
    
    init() {
        urlSessionConfig = URLSessionConfiguration.default
        urlSessionConfig.allowsCellularAccess = true
        if #available(iOS 11.0, *) {
            urlSessionConfig.multipathServiceType = .handover
        }
    }
    
    open func parseJson<T: Decodable>(fromData jsonData: Data) -> T? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            return try decoder.decode(T.self, from: jsonData)
        }
        catch let decodeError as NSError {
            print("Decoding error: \(decodeError.localizedDescription)")
            return nil
        }
    }
    
    func get<T: Decodable>(_ endpoint: String, complete: @escaping (T?) -> Void, queryItems: [URLQueryItem] = []) {
        
        DispatchQueue(label: "getRequest").async {
            
            var urlComponents = URLComponents(string: ApiConfig.baseUrl + endpoint)!
            urlComponents.queryItems = queryItems
            
            let url = urlComponents.url!
            var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: ApiConfig.timeout)
            urlRequest.httpMethod = "GET"
            urlRequest.addValue("application/json", forHTTPHeaderField: "content-type")
            
            let urlSession = URLSession(configuration: .default)
            print("DataTask reguest: \(String(describing: urlRequest.url))")
            let task = urlSession.dataTask(with: urlRequest) { data, response, error in
                let stringData = String(data: data!, encoding: String.Encoding.utf8)
                print("DataTask response: \(String(describing: response))\n" +
                    "with data: \(String(describing: stringData))")
                var result: T? = nil
                
                if let error = error {
                    print("DataTask error: \(error.localizedDescription)")
                }
                else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    result = self.parseJson(fromData: data)
                }
                
                DispatchQueue.main.async {
                    complete(result)
                }
            }
            task.resume()
            
        }
    }
}
