//
//  File.swift
//  Tunny
//
//  Created by Pavel Chupryna on 16/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setHidden(_ isHide: Bool, animated: Bool = true) {
        if self.isHidden == isHide {
            return
        }
        
        if animated {
            self.alpha = isHide ? 1 : 0
            UIView.animate(withDuration: 0.23, animations: {
                self.alpha = isHide ? 0 : 1
            }) { _ in
                self.isHidden = isHide
            }
        }
        else {
            self.alpha = 1
            self.isHidden = isHide
        }
    }
}
