//
//  AlbumTableViewSource.swift
//  Tunny
//
//  Created by Pavel Chupryna on 16/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation
import UIKit

class AlbumTableViewSource: NSObject, UITableViewDataSource {
    
    var items: [Song]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SongTableViewCell.key, for: indexPath) as! SongTableViewCell
        
        let item = items![indexPath.row]
        cell.fillBySong(item, row: indexPath.row)
        
        return cell
    }
}
