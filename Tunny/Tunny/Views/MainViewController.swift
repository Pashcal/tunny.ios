//
//  MainViewController.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UISearchBarDelegate {
    
    private let searchService = SearchService()

    private let collectionViewSource = MainCollectionViewSource()
    
    @IBOutlet weak private var searchBar: UISearchBar!
    @IBOutlet weak private var emptyView: UILabel!
    @IBOutlet weak private var collectionView: UICollectionView!
//    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Tunny"
        
        initSearchBar()
        initCollectionView()
        
        searchBar.becomeFirstResponder()
    }
    
    // MARK: - SearchBar tuning
    
    func initSearchBar() {
        searchBar.backgroundImage = UIImage.imageWithColor(color: .clear)
        
        let searchField = searchBar.value(forKey: "searchField") as! UITextField
        searchField.backgroundColor = .groupTableViewBackground
        searchField.alpha = 0.98
        
        searchBar.delegate = self
    }
    
    // MARK: SearchBar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            loadAlbums(by: searchText)
        }
    }
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(true, animated: true)
//    }
    
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(false, animated: true)
//    }
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchBar.endEditing(true)
//        self.updateCollectionBy([])
//    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchQuery = searchBar.text else {
            return
        }
        
        searchBar.endEditing(true)
//        self.collectionViewSource.items = []
//        self.collectionView.reloadData()
//        activityIndicator.isHidden = false
        
        loadAlbums(by: searchQuery)
    }
    
    // MARK: - Collection view tuning
    
    func initCollectionView() {
        collectionView.contentInset.top = searchBar.bounds.height
        
        collectionView.register(AlbumCollectionViewCell.nib, forCellWithReuseIdentifier: AlbumCollectionViewCell.key)
        
        updateCollectionItemSize()
        
        collectionViewSource.itemSelected = onItemSelected(item:)
        collectionView.dataSource = collectionViewSource
        collectionView.delegate = collectionViewSource
    }
    
    func updateCollectionItemSize() {
        
        var maxWidth = UIScreen.main.bounds.width
        if #available(iOS 11.0, *), let window = UIApplication.shared.keyWindow {
            maxWidth = window.safeAreaLayoutGuide.layoutFrame.size.width
        }
        
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellsInRow: CGFloat = UIDevice.current.orientation.isPortrait ? 2 : 4
        let cellWidth = (maxWidth - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right - collectionViewLayout.minimumInteritemSpacing * (cellsInRow - 1)) / cellsInRow
        UIView.animate(withDuration: 0.23) {
            collectionViewLayout.itemSize = CGSize(width: cellWidth, height: cellWidth * 1.35)
        }
        
        collectionViewLayout.invalidateLayout()
    }
    
    // have glitches
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        coordinator.animate(alongsideTransition: { (context) in
//            //without custom animation
//        }) { (context) in
//            self.updateCollectionItemSize()
//        }
//    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateCollectionItemSize()
    }
    
    // MARK: - Load and fill CollectionView by albums
    
    func loadAlbums(by searchQuery: String) {
        searchService.getAlbums(by: searchQuery) { albums in
//            self.activityIndicator.isHidden = true
            self.updateCollection(by: albums)
        }
    }
    
    func updateCollection(by albums: [Album]?) {
        
        let isEmpty = albums?.isEmpty ?? true
        emptyView.setHidden(!isEmpty)
        
        self.collectionViewSource.items = albums
        self.collectionView.reloadData()
    }
    
    // MARK: Navigate to details
    
    func onItemSelected(item: Album) {
        navigationController?.pushViewController(AlbumViewController(album: item), animated: true)
    }
}
