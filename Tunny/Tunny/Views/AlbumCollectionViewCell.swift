//
//  AlbumCollectionViewCell.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import UIKit
import Nuke

class AlbumCollectionViewCell: UICollectionViewCell {

    static let key = "AlbumCell"
    static let nib = UINib(nibName: "AlbumCollectionViewCell", bundle: nil)
    
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var singerLabel: UILabel!
    @IBOutlet weak private var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 4
    }
    
    func fillByAlbum(_ album: Album) {
        
        let imageWidth = Int(contentView.bounds.width * UIScreen.main.scale)
        let imageUrl = URL(string: album.artworkUrlWith(width: imageWidth))!
        Nuke.loadImage(with: imageUrl, into: imageView)
        
        titleLabel.text = album.collectionName
        singerLabel.text = album.artistName
    }
}
