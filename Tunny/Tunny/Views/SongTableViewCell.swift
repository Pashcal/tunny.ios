//
//  SongTableViewCell.swift
//  Tunny
//
//  Created by Pavel Chupryna on 16/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    static let key = "SongCell"
    static let nib = UINib(nibName: "SongTableViewCell", bundle: nil)
    
    @IBOutlet weak private var numberLabel: UILabel!
    @IBOutlet weak private var songLabel: UILabel!
    @IBOutlet weak private var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func fillBySong(_ song: Song, row: Int) {
        numberLabel.text = String(row + 1) //String(song.trackNumber)
        songLabel.text = song.trackName
        
        if let trackTimeMillis = song.trackTimeMillis {
            let date = Date(timeIntervalSince1970: TimeInterval(trackTimeMillis / 1000))
            let df = DateFormatter()
            df.dateFormat = "m:ss"
            timeLabel.text = df.string(from: date)
        }
        else {
            timeLabel.text = ""
        }
    }
}
