# Tunny

## Task description
In the test task, you need to develop a simple application for the iPhone. The application should display
album artwork from the iTunes API, and the user should be able to see detailed information about the
selected album.

#### UI Description:
* UISearchBar for start and stop searching
* UICollectionView with albums
* after selecting an album, you need to display a screen with full information about the album and list of songs

#### Other requirements:
* must use the latest Xcode 9 + Swift 4, iOS 10+
* navigation between screens
* albums have to be sorted alphabetically
* API description: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/

## Technology stack

##### Core
* Swift
* Foundation
* Grand Central Dispatch (GCD)
* MVC based architecture

##### API
* URLSession
* Codable classes
* Services for REST API endpoints

##### UI
* UIKit
* Core Graphics
* Core Animation
* Interface Builder and Auto Layouts
* NavBar based navigation

##### CocoaPods
* Nuke for loading images
