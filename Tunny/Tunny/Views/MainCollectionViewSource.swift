//
//  MainCollectionViewSource.swift
//  Tunny
//
//  Created by Pavel Chupryna on 14/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation
import UIKit

class MainCollectionViewSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var items: [Album]?
    var itemSelected: ((Album) -> Void)?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionViewCell.key, for: indexPath) as! AlbumCollectionViewCell
        
        let item = items![indexPath.row]
        cell.fillByAlbum(item)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let itemSelected = itemSelected, let items = items, indexPath.row < items.count else {
            return
        }
        
        itemSelected(items[indexPath.row])
    }
}
