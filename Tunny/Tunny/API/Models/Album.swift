//
//  Album.swift
//  Tunny
//
//  Created by Pavel Chupryna on 10/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation

class Album: Codable {
    let wrapperType: String?
    let collectionType: String?
    let artistID: Int?
    let collectionID: Int?
    let amgArtistID: Int?
    let artistName: String?
    let collectionName: String?
    let collectionCensoredName: String?
    let artistViewURL: String?
    let collectionViewURL: String?
    let artworkUrl60: String?
    let artworkUrl100: String?
    let collectionPrice: Double?
    let collectionExplicitness: String?
    let contentAdvisoryRating: String?
    let trackCount: Int?
    let copyright: String?
    let country: String?
    let currency: String?
    let releaseDate: Date?
    let primaryGenreName: String?
    
    enum CodingKeys: String, CodingKey {
        case wrapperType = "wrapperType"
        case collectionType = "collectionType"
        case artistID = "artistId"
        case collectionID = "collectionId"
        case amgArtistID = "amgArtistId"
        case artistName = "artistName"
        case collectionName = "collectionName"
        case collectionCensoredName = "collectionCensoredName"
        case artistViewURL = "artistViewUrl"
        case collectionViewURL = "collectionViewUrl"
        case artworkUrl60 = "artworkUrl60"
        case artworkUrl100 = "artworkUrl100"
        case collectionPrice = "collectionPrice"
        case collectionExplicitness = "collectionExplicitness"
        case contentAdvisoryRating = "contentAdvisoryRating"
        case trackCount = "trackCount"
        case copyright = "copyright"
        case country = "country"
        case currency = "currency"
        case releaseDate = "releaseDate"
        case primaryGenreName = "primaryGenreName"
    }
    
    init(wrapperType: String, collectionType: String, artistID: Int, collectionID: Int, amgArtistID: Int, artistName: String, collectionName: String, collectionCensoredName: String, artistViewURL: String, collectionViewURL: String, artworkUrl60: String, artworkUrl100: String, collectionPrice: Double?, collectionExplicitness: String, contentAdvisoryRating: String?, trackCount: Int, copyright: String, country: String, currency: String, releaseDate: Date, primaryGenreName: String) {
        self.wrapperType = wrapperType
        self.collectionType = collectionType
        self.artistID = artistID
        self.collectionID = collectionID
        self.amgArtistID = amgArtistID
        self.artistName = artistName
        self.collectionName = collectionName
        self.collectionCensoredName = collectionCensoredName
        self.artistViewURL = artistViewURL
        self.collectionViewURL = collectionViewURL
        self.artworkUrl60 = artworkUrl60
        self.artworkUrl100 = artworkUrl100
        self.collectionPrice = collectionPrice
        self.collectionExplicitness = collectionExplicitness
        self.contentAdvisoryRating = contentAdvisoryRating
        self.trackCount = trackCount
        self.copyright = copyright
        self.country = country
        self.currency = currency
        self.releaseDate = releaseDate
        self.primaryGenreName = primaryGenreName
    }
}

extension Album {
    
    func artworkUrlWith(width: Int) -> String {
        return artworkUrl100?.replacingOccurrences(of: "100x100bb", with: "\(width)x\(width)bb") ?? ""
    }
}
