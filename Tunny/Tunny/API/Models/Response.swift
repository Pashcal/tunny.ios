//
//  Result.swift
//  Tunny
//
//  Created by Pavel Chupryna on 11/06/2018.
//  Copyright © 2018 Pavel Chupryna. All rights reserved.
//

import Foundation

class Response<T: Decodable>: Decodable {
    let resultCount: Int
    let results: [T]
    
    enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case results = "results"
    }
    
    init(resultCount: Int, results: [T]) {
        self.resultCount = resultCount
        self.results = results
    }
}
